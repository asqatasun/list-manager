<?php

namespace App\Command;

use App\Entity\Url;
use App\Entity\UrlList;
use Doctrine\ORM\EntityManagerInterface;
use League\Csv\Exception;
use League\Csv\Reader;
use League\Csv\UnavailableStream;
use Symfony\Component\Console\Attribute\AsCommand;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;

#[AsCommand(
    name: 'app:csv:import',
    description: 'Import URLs and lists from a CSV file',
)]
class CsvImportCommand extends Command
{
    private EntityManagerInterface $em;

    public function __construct(EntityManagerInterface $entityManager)
    {
        parent::__construct();
        $this->em = $entityManager;
    }

    protected function configure(): void
    {
//        $this
//            ->addArgument('arg1', InputArgument::OPTIONAL, 'Argument description')
//            ->addOption('option1', null, InputOption::VALUE_NONE, 'Option description');
    }

    /**
     * @throws UnavailableStream
     * @throws Exception
     */
    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $io = new SymfonyStyle($input, $output);
        $io->title('Attempting import of Feed...');

        $reader = Reader::createFromPath('%kernel.project_dir%/../var/MOCK_DATA.csv', 'r');
        $reader->setHeaderOffset(0); // removes header from CSV file
        $records = $reader->getRecords(['url', 'http_status', 'https_status', 'siren', 'urlList', 'source', 'script']);

        $io->progressStart(iterator_count($records));

        foreach ($records as $offset => $record) {

            // Check URL does not already exists in DB
            $url = $this->em->getRepository(Url::class)
                ->findOneBy([
                    'url' => $record['url']
                ]);

            // Add URL
            if ($url === null) {
                $url = (new Url())
                    ->setUrl($record['url']);
                $this->em->persist($url);
                $this->em->flush();
            }

            // Deal with urlList and the association to the URL
            if ($record['urlList']) {
                // Check urlList does not already exists in DB
                $urlList = $this->em->getRepository(urlList::class)
                    ->findOneBy([
                        'name' => $record['urlList']
                    ]);

                // Add urlList
                if ($urlList === null) {
                    $urlList = (new urlList())
                        ->setName($record['urlList']);
                    $this->em->persist($urlList);
                    $this->em->flush();
                }

                // Associate the URL to its list
                $urlListCollection = $url->getUrlList();
                if ( ! $urlListCollection->contains([$record['urlList']]) ) {
                    $url->addUrlList($urlList);
                }

                $this->em->flush();

                $io->progressAdvance();
            }
        }
//        $arg1 = $input->getArgument('arg1');
//
//        if ($arg1) {
//            $io->note(sprintf('You passed an argument: %s', $arg1));
//        }
//
//        if ($input->getOption('option1')) {
//            // ...
//        }

        $io->progressFinish();
        $io->success('URLs and lists imported.');

        return Command::SUCCESS;
    }
}
