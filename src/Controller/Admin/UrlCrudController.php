<?php

namespace App\Controller\Admin;

use App\Entity\Url;
use EasyCorp\Bundle\EasyAdminBundle\Config\Crud;
use EasyCorp\Bundle\EasyAdminBundle\Config\Filters;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;
use EasyCorp\Bundle\EasyAdminBundle\Field\AssociationField;
use EasyCorp\Bundle\EasyAdminBundle\Field\DateTimeField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextField;
use EasyCorp\Bundle\EasyAdminBundle\Filter\EntityFilter;

class UrlCrudController extends AbstractCrudController
{
    public static function getEntityFqcn(): string
    {
        return Url::class;
    }

    public function configureCrud(Crud $crud): Crud
    {
        return $crud
            ->setEntityLabelInSingular('URL')
            ->setEntityLabelInPlural('URLs')
            ->setSearchFields(['url', 'origin', 'name'])
            ->setDefaultSort(['createdAt' => 'DESC']);
    }

    public function configureFilters(Filters $filters): Filters
    {
        return $filters
            ->add(EntityFilter::new('urlList'));
    }

    public function configureFields(string $pageName): iterable
    {
        yield TextField::new('url');
        yield TextField::new('name');
        yield TextField::new('origin');
        yield AssociationField::new('urlList');

        if (Crud::PAGE_EDIT === $pageName) {
            yield DateTimeField::new('createdAt')
                ->setFormTypeOption('disabled', true);
        }
    }

}
