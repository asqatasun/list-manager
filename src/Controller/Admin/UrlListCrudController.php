<?php

namespace App\Controller\Admin;

use App\Entity\UrlList;
use EasyCorp\Bundle\EasyAdminBundle\Config\Crud;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;
use EasyCorp\Bundle\EasyAdminBundle\Field\DateTimeField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextField;

class UrlListCrudController extends AbstractCrudController
{
    public static function getEntityFqcn(): string
    {
        return UrlList::class;
    }

    public function configureCrud(Crud $crud): Crud
    {
        return $crud
            ->setEntityLabelInSingular('URL List')
            ->setEntityLabelInPlural('URL Lists')
            ->setSearchFields(['name', 'description'])
            ->setDefaultSort(['name' => 'ASC']);
    }

    public function configureFields(string $pageName): iterable
    {
        yield TextField::new('name');
        yield TextField::new('description');

        if (Crud::PAGE_EDIT === $pageName) {
            yield DateTimeField::new('createdAt')
                ->setFormTypeOption('disabled', true);
        }
    }
}
