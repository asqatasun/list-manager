# Prerequisites

- [Composer](https://getcomposer.org/)
- PHP 8.1 + extensions

### PHP extensions needed

- php-intl
- php-mbstring
- php-pdo_pgsql
- php-xml


