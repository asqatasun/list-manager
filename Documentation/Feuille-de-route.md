# Feuille de route

À terme, List-Manager comporte plusieurs parties.

## Vision

### FrontOffice

Le *frontoffice* est la partie destinée aux utilisatrices finales. Elle permet de :

- saisir de nouvelles URLs, leur associer un (ou plusieurs) catégories
- visualiser les catégories / listes

### BackOffice

Le *backoffice* permet aux utilisatrices expérimentées de :

- amender les saisies opérées par les contributrices
- exporter au format adéquat les contributions (URLs réduites en domaine) afin de les importer dans le dépôt
  des [noms de domaines des organismes du secteur public](https://gitlab.adullact.net/dinum/noms-de-domaine-organismes-secteur-public/)

### Ludification

Afin de rendre ludique les opérations d'ajout de domaine / catégorie, il est prévu pour les utilisateurs le souhaitant
de se créer un compte pour :

- suivre son nombre de points (par exemple : un domaine saisi = 1 point ; un domaine saisi pas encore en base = 2
  points)
- bénéficier de nouvelles habilitations selon le nombre de points acquis. Par exemple, à partir de 50 points, je peux
  modifier les catégories
- visualiser les badges gagnés en fonction des actions opérées. Exemples :
    - à 10 domaines saisis, j'ai le badge "contributeur" ; à 50, le badge "arpenteur" ; à 100, le badge "bibliothécaire"
    - à 3 domaines replacés dans une autre catégorie, j'ai le badge "jardinier", à 20, le badge "terraformeur"

Sources d'inspiration :

- StackOverflow :
    - [StackOverflow privileges](https://stackoverflow.com/help/privileges)
    - [StackOverflow badges](https://stackoverflow.com/help/badges)
- [StreetComplete achievements](https://wiki.openstreetmap.org/wiki/User:Westnordost/StreetComplete/Achievements#Achievements)
- Discourse (qui affiche tous les badges, afin de donner envie d'aller au prochain)
    - [Discourse: What are Badges?](https://meta.discourse.org/t/what-are-badges/32540)
    - [https://meta.discourse.org/t/what-are-badges/32540](https://meta.discourse.org/t/initial-discourse-badge-design-spec/13088)
- [La gamification : comprendre le principe et l’appliquer](https://edusign.fr/blog/la-gamification-definition-et-principe/)

### Catégories

Les URLs sont rangées dans des catégories (ou listes). Elles peuvent être hiérarchiques (catégorie / sous-catégorie).
Exemples :

- L'URL de l'[université de Montpellier](https://www.umontpellier.fr/) appartient à la catégorie *Enseignement
  supérieur*, fille de *Ministère de l'Enseignement Supérieur et de la Recherche*, elle-même fille de *Ministère*
- L'URL de la commune de [Mons-la-Trivalle](https://www.monslatrivalle.fr/) peut appartenir à la liste des communes
  rurales, et aussi à la liste des communes de l'Hérault.

### Cardinalités

- une URL *peut* appartenir à une catégorie
- une URL *peut* appartenir à plusieurs catégories
- une catégorie *peut* avoir plusieurs catégories filles
- une catégorie ne peut avoir *qu'une seule* catégorie parente 

## Version 0.1

- seuls les [domaines](https://fr.wikipedia.org/wiki/Nom_de_domaine) sont gérés (
  les [URLs](https://fr.wikipedia.org/wiki/Uniform_Resource_Locator) sont automatiquement converties en domaine)
- le *frontoffice* permet de saisir une URL, et de lui associer une (seule) catégorie
- le *backoffice* est inexistant
- une contributrice est systématiquement remerciée pour son ajout
- la contributrice est informée si le domaine est déjà existant en base
- la base de données est alimentée depuis le
  dépôt [noms de domaines des organismes du secteur public](https://gitlab.adullact.net/dinum/noms-de-domaine-organismes-secteur-public/),
  la date de dernière alimentation est affichée à la contributrice.
- une documentation technique permet à une développeuse d'ajouter les domaines au
  dépôt [noms de domaines des organismes du secteur public](https://gitlab.adullact.net/dinum/noms-de-domaine-organismes-secteur-public/)

## Versions futures

- Le formulaire de saisie d'URL présente une case à cocher "je souhaite une relecture par une personne expérimentée" (
  inspiration de l'éditeur *ID* d'OpenStreetMap)
- gestion des listes et catégories
- Tout ce qui est décrit ci-dessus.