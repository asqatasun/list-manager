# List Manager

List-manager manages lists of URLs. 

## Visuals

(to be done)

## Installation

(to be done)

## Roadmap

See [*Feuille de route*](Documentation/Feuille-de-route.md)

## Contributing

Pull requests are welcome. For major changes, please open an issue first
to discuss what you would like to change.

Please make sure to update tests as appropriate.

## Authors and acknowledgment

Matthieu Faure, ADULLACT

## License

[AGPL 3.0 or later](https://choosealicense.com/licenses/agpl-3.0/)

## Project status

Under development
